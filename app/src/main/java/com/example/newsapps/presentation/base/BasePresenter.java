package com.example.newsapps.presentation.base;

public interface BasePresenter<View> {
    void onStartView(View view);
    void onStopView();
}
