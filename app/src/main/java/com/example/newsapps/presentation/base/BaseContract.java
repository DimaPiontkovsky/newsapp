package com.example.newsapps.presentation.base;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;


public interface BaseContract {
    void goToFragment(Fragment fragment, int container);
    void openDialog(DialogFragment fragment, int container);
    void closeFragmentDialog(DialogFragment fragment);
}
