package com.example.newsapps.presentation.activity.activity_splash;

public class SplashPresenter implements SplashView.Presenter {
    SplashView.View view;
    @Override
    public void onStartView(SplashView.View view) {
        this.view = view;
    }

    @Override
    public void onStopView() {
        view = null;
    }
}
