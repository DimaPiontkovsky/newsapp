package com.example.newsapps.presentation.activity.activity_splash;

import com.example.newsapps.presentation.base.BaseContract;
import com.example.newsapps.presentation.base.BasePresenter;

public interface SplashView {
    interface View extends BaseContract {

    }

    interface Presenter extends BasePresenter<View>{

    }
}
