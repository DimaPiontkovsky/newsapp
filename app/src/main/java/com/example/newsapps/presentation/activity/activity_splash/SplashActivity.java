package com.example.newsapps.presentation.activity.activity_splash;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import com.example.newsapps.R;
import com.example.newsapps.databinding.ActivitySplashBinding;
import com.example.newsapps.presentation.base.BaseActivity;
import com.example.newsapps.presentation.base.BasePresenter;

public class SplashActivity extends BaseActivity<ActivitySplashBinding>  implements SplashView.View{
    private SplashView.Presenter presenter;


    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
        presenter = new SplashPresenter();
        getBinding().setEvent(presenter);
    }

    @Override
    protected int getResLayout() {
        return R.layout.activity_splash;
    }

    @Override
    protected void startView() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void goToFragment(Fragment fragment, int container) {
        super.goToFragmentWithBackStack(fragment, container);
    }

    @Override
    public void openDialog(DialogFragment fragment, int container) {

    }

    @Override
    public void closeFragmentDialog(DialogFragment fragment) {

    }
}