package com.example.newsapps.presentation.router;

import com.example.newsapps.data.model.Data;
import com.example.newsapps.presentation.base.BaseContract;

public interface IRouter {

    void startView(BaseContract view);
    void stopView( );
    void step(String tag, Data data);

}

