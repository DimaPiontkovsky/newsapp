package com.example.newsapps.presentation.router;

import com.example.newsapps.data.model.Data;
import com.example.newsapps.presentation.base.BaseContract;

public class Router implements IRouter {
    private static IRouter instance;
    private BaseContract view;

    private Router() {
    }

    public static synchronized IRouter getInstance() {
        if (instance == null) {
            instance = new Router();
        }
        return instance;
    }

    @Override
    public void startView(BaseContract view) {
        this.view = view;
    }

    @Override
    public void stopView() {
        view = null;
    }

    @Override
    public void step(String tag, Data data) {
        switch (tag) {
            case "1":
                break;
        }

    }
}
