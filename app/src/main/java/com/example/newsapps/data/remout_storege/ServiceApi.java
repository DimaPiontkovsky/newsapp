package com.example.newsapps.data.remout_storege;

import com.example.newsapp.data.model.api.country.ResponseCountry;
import com.google.gson.Gson;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface ServiceApi {

    Api getApi();

    Gson getGson();

    interface Api {
        @GET("/v2/top-headlines")
        Single<ResponseCountry> requestTopHeadlinesCountry(@Query("country") String country, @Header("Authorization") String authorization);

        @GET("/v2/everything")
        Single<ResponseCountry> requestSearch(@Query(value = "q", encoded = true) String query,
                                           @Query(value = "from", encoded = true) String from,
                                           @Query(value = "to", encoded = true) String to,
                                           @Query(value = "apiKey", encoded = true) String authorization);

        @GET("/v2/top-headlines")
        Single<ResponseCountry> requestNewsChannel(@Query(value = "sources", encoded = true) String sources, @Header("Authorization") String authorization);

        @GET("/v2/sources")
        Single<ResponseCountry> requestGroup(@Header("Authorization") String authorization);

        @GET("/v2/sources")
        Single<ResponseCountry> requestLanguage(@Query("language") String language, @Header("Authorization") String authorization);
    }
}
