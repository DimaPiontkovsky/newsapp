package com.example.newsapps.data.model.dao;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "news_country")
public class EntityNews {
    @PrimaryKey(autoGenerate = true)
    private long id;

    public EntityNews() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
