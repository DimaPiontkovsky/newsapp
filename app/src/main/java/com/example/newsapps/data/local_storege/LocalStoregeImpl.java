package com.example.newsapps.data.local_storege;

import android.content.Context;

import androidx.room.Room;

import com.example.newsapp.Constanta;

public class LocalStoregeImpl implements LocalStorege {

    private LocalDAO api;
    private static LocalStorege instance;

    private LocalStoregeImpl(Context context) {
        api = initDao(context).getPersonDao();

    }


    public static synchronized LocalStorege getInstance(Context... contexts) {
        if (instance == null && contexts != null && contexts[0] != null) {
            instance = new LocalStoregeImpl(contexts[0]);
        }
        return instance;
    }

    private DataBase initDao(Context context){
        return Room.databaseBuilder(context,DataBase.class, Constanta.NAME_DAO)
                .build();
    }
}
